"""
This script is to demonstrate certain examples for 
retrieving data from 123RF's images table
"""
import sys 
sys.path.append('../../')
from sado.wantoe3images import ImagesRange, ImagesLatest, ImagesFromList 

if __name__ == '__main__':
	# Calling images ranging from image_id `start_id` till `end_id`
	df_range = ImagesRange(
		start_id=100000000,
		end_id=100000100,
		columns=['image_id', 'keywords', 'image_desc'],
		show_url=True,
		batch_size=32,
		show_image=True,
		connect_timeout=5,
		read_timeout=10)

	for i in df_range.one_shot_iterator():
		print(i.shape)

	# Calling the #`latest` images 
	df_latest = ImagesLatest(
		latest=1000, 
		columns=['image_id', 'keywords', 'image_desc'],
		show_url=True,
		show_image=True)
	for i in df_latest.one_shot_iterator():
		print(i.head())

	# Calling images from specific image_ids
	df_list = ImagesFromList(
		image_ids=[100000000, 100000001, 100000002, 100000003],
		columns=['image_id', 'keywords', 'image_desc'],
		show_url=True,
		show_image=True)
	for i in df_list.one_shot_iterator():
		print(i.head())