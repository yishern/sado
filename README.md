# sado

This repository contains some utilities which may be reusable across various research projects 

## Installation
```pip install -r requirements.txt```

### Pillow installation

#### Conda 
```
conda uninstall --force jpeg libtiff -y
conda install -c conda-forge libjpeg-turbo
CC="cc -mavx2" pip install --no-cache-dir -U --force-reinstall --no-binary :all: --compile pillow-simd
```
If you only care about faster JPEG decompression, it can be `pillow` or `pillow-simd` in the last command above, the latter speeds up other image processing operations.

#### Pip
```
pip uninstall pillow
CC="cc -mavx2" pip install -U --force-reinstall pillow-simd
```

## Tools
### wantoe3images
This section contains utilities (ImagesRange, ImagesLatest, ImagesFromList) to retrieve data from 123RF's images table. Refer to this [link](https://bitbucket.org/yishern/sado/src/master/examples/data_loading.py) for examples on how to use them 