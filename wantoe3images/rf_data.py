import json 
from easydict import EasyDict 
from multiprocessing import cpu_count 
from .process_utils import * 
from .dataframe_utils import * 
import sys 
import os 
import functools 

try:
	default_config_file_path =  os.path.join(os.path.dirname(os.path.realpath(__file__)), 'default.json')
	default = EasyDict(json.load(open(default_config_file_path,'r')))
except:
	sys.exit('File {filename} is not found'.format(filename=default_config_file_path))

class Images:
	def __init__(self, credentials=None, engine_config=None, max_queryrange=10000, image_base_url=default.image_base_url, get_image_query=default.basequery, start_id=None, end_id=None, batch_size=100, latest=None, list_ids=None, columns=[], show_url=False, show_image=False, max_workers=1, verbosity=False, connect_timeout=10, read_timeout=10):
		self.credentials = credentials
		self.engine_config = engine_config 
		self.create_engine()
		assert max_queryrange <=10000, 'It is advisable that `max_queryrange` is kept below 10000'
		assert batch_size <= max_queryrange 
		self.max_queryrange = max_queryrange 
		self.image_base_url = image_base_url
		self.get_image_query = get_image_query
		self.batch_size = batch_size
		self.columns = columns 
		self.show_url = show_url 
		self.show_image=show_image 
		self.max_workers = max_workers 
		self.verbosity = verbosity
		self.add_image_object_with_timeout = functools.partial(add_image_object, read_timeout=read_timeout, connect_timeout=connect_timeout)
		self.display()

	def display(self):
		self.display_later = self.columns 
		if self.show_url:
			self.display_later.append('url')
		if self.show_image:
			self.display_later.append('image')

	def create_engine(self):
		self.credentials = self.credentials if self.credentials is not None else default.db_engine.credentials 
		self.engine_config = self.engine_config if self.engine_config is not None else default.db_engine.engine_config 
		self.engine = create_db_engine(**self.credentials, **self.engine_config)

	def one_shot_iterator(self):
		pass 

	def edit_return(self, df):
		if df.shape[0] != 0:
			if self.show_url:
				df = construct_image_url(df, base_url=self.image_base_url)
			if self.show_image:
				if 'url' not in df.columns:
					df = construct_image_url(df, base_url=self.image_base_url)

				if self.max_workers==1:
					df = self.add_image_object_with_timeout(df)
				else:
					df = parallelize(df, self.add_image_object_with_timeout, cores=self.max_workers)
			if self.columns is not None:
				df = df.loc[:, self.display_later]
		return df 

class ExtRangeImages(Images):
	def get_image_data(self, params):
		return pd.read_sql(self.get_image_query, con=self.engine.engine, params=params)

	def _aux_one_shot_iterator(self, start_id, end_id):
		for i in range(start_id, end_id, self.batch_size):
			if i + self.batch_size >= self.end_id:
				up_bound = self.end_id 
			else:
				up_bound = i + self.batch_size 
			if self.verbosity:
				print('Retrieving data from images table from image_id{} to {}'.format(i, up_bound))
			params= {'low_bound_image_id': i, 'up_bound_image_id': up_bound}
			df = self.get_image_data(params)
			df = self.edit_return(df)
			yield df 

class ImagesRange(ExtRangeImages):
	def __init__(self, start_id, end_id, **kwargs):
		self.start_id = start_id
		self.end_id = end_id 
		ExtRangeImages.__init__(self, **kwargs)

	def one_shot_iterator(self):
		return self._aux_one_shot_iterator(self.start_id, self.end_id)

class ImagesLatest(ExtRangeImages):
	def __init__(self, latest, **kwargs):
		self.latest = latest 
		ExtRangeImages.__init__(self, **kwargs)
		self.get_bound()
		
	def get_bound(self):
		self.end_id = self.engine.execute("SELECT MAX(image_id) FROM images;").fetchone()[0]
		self.start_id = self.end_id - self.latest 

	def one_shot_iterator(self):
		return self._aux_one_shot_iterator(self.start_id, self.end_id)

class ImagesFromList(Images):
	def __init__(self, image_ids, **kwargs):
		self.image_ids = image_ids 
		Images.__init__(self, get_image_query=default.get_list_query, **kwargs)

	def get_image_data(self, query):
		return pd.read_sql(query, con=self.engine, params=None)

	def one_shot_iterator(self):
		for batch in chunker(self.image_ids, self.batch_size):
			query = self.get_image_query.format(id_list=','.join(map(str, batch)))
			df = self.get_image_data(query)
			df = self.edit_return(df)
			yield df 
