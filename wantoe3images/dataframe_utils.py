import pandas as pd 
import requests 
from PIL import Image 
from io import BytesIO 
import numpy as np 
import multiprocessing as mp 


def parallelize(data, func, cores=10):
	"""
	Multiprocessing by splitting pandas.DataFrame to apply function on each row 
	Args:
		data (pandas.DataFrame): Dataframe to be processed
		func (func): Function to be applied on each row 
		cores (int): Number of workers to be used for processing
	Returns:
		data (pandas.DataFrame): post-processed dataframe)
	"""
	partitions = cores 
	data_split = np.array_split(data, partitions)
	pool = mp.Pool(cores)
	data = pd.concat(pool.map(func, data_split))
	pool.close()
	pool.join()
	return data 

def download(url, connect_timeout, read_timeout):
	"""
	Create PIL.Image object from downloading URL 
	Args:
		url (str): Url of the image
	Returns:
		img (PIL.Image): Image object of the URL 
	"""
	try:
		response = requests.get(url, timeout=(connect_timeout, read_timeout))
		img = Image.open(BytesIO(response.content)).convert('RGB')
		return img 
	except:
		return None 
		
def add_image_object(df, connect_timeout=30, read_timeout=30):
	"""
	Create new column on dataframe to apply function `download`
	Args:
		df (pandas.DataFrame): Dataframe where download function is applied on each row
	Returns:
		df (pandas.DataFrame): Post processed dataframe
	"""
	df['image'] = df['url'].apply(lambda x: download(x, connect_timeout=connect_timeout, read_timeout=read_timeout))
	return df 

def construct_image_url(df, base_url):
	"""
	Construct url base on dataframe collected information 
	Args:
		df (pandas.DataFrame): dataframe to be processed
		base_url (str): Base url with keys stored as the columns of the dataframe 
	Returns:
		df (pandas.DataFrame): dataframe with image_url constructed based on the image meta information
	"""
	def construct(row):
		try:
			return base_url.format(**dict(row))
		except:
			return None 
	df['url'] = df.apply(construct, axis=1)
	return df 