from sqlalchemy import create_engine 

def create_db_engine(host, user, password, dbname, database, driver, port, pool_recycle, pool_size, encoding):
	"""
	Create database engine from sqlalchemy (dialect+driver://username:password@host:port/database)
	Args:
		host (str)
		user (str)
		password (str)
		dbname (str)
		database(str)
		driver (str)
		port (str)
		pool_recycle (int)
		pool_size (int)
		encoding (str)
	Returns:
		engine (sqlalchemy.engine.Engine): engine that will provide connection objects with the given execution options
	"""
	credentials = {'host': host, 'user': user, 'password': password, 'dbname': dbname, 'database': database, 'driver': driver, 'port': port}
	settings = {'pool_recycle': pool_recycle, 'pool_size': pool_size, 'encoding': encoding}
	engine = create_engine('{database}+{driver}://{user}:{password}@{host}:{port}/{dbname}'.format(**credentials), **settings)
	return engine 

def chunker(seq, size):
	"""
	Partition list into batches of required size
	Args:
		seq (list): List to be partitioned
		size (int): Size of each partition
	Return:
		A list of partitions
	"""
	return (seq[pos:pos + size] for pos in range(0, len(seq), size))