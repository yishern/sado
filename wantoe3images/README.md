# wantoe3images

This implementation includes fast retrieval of images from 123RF images table

* ImagesRange returns data within a certain range of image_id specified
* ImagesLatest returns the n latest images
* ImagesFromList returns the data given a list of image_id.